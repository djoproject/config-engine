* copy the binnaries from extensions ? make a symbolic link ? currently copying.
* extension installation: copy extension or make symbolic link ?
    what if symbolic link is broken ?
    with copy, still have git information to update
    just clone extension into .custom_config directory
    allow both to install both repository and git repository
        in case of local directory, create a symbolic link into the extensions directory
        in case of a git repository, clone it into the extension directory
